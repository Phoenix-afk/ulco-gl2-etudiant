#pragma once
#include "task.hpp"
#include "board.hpp"
#include <iostream>
#include <list>
#include <iterator>
using namespace std;

class View{
    void showTask(list <Task> tasks);
    void printBoard(Board board);
}