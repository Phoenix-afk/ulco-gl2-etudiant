module Board where

-- import Data.List (partition)
import Task

data Board = Board
    { _boardId :: Int
    , _boardTodo :: [Task]
    , _boardDone :: [Task]
    }

newBoard :: Board
newBoard = Board 1 [] []


addTodo :: String -> Board -> (Int, Board)
addTodo str (Board i ts ds) = 
    let task = Task i str
        board = Board (i + 1) (ts++[task]) ds
    in (i, board)
    -- TODO

toDone :: Int -> Board -> Board
toDone i0 (Board _i ts ds) = 
    let (x,xs) = splitAt (i0) ts
        (xs1,xs2) = splitAt 1 xs
        board = Board _i (x++xs2) (ds++xs1)
    in board
    -- TODO