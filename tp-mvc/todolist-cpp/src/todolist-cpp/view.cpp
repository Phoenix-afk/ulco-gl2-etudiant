#pragma once

#include "view.hpp"

void View::showTask(list <Task> task){
    list <Task> :: iterator it;
    for(it = task.begin(); it != task.end(); ++it)
        cout << '\n' << *it;
}

void View::printBoard(Board board){
    cout << board << endl;
}